import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.hooks.ListenerAdapter;


import javax.security.auth.login.LoginException;


public class Connection extends ListenerAdapter{
    public static void main (String[] args){
        JDABuilder builder = new JDABuilder(AccountType.BOT);
        builder.setGame(Game.playing("with Sleep7"));
        builder.setToken(Constants.discordToken);

        JDA jda = null;
        try{
            jda = builder.buildAsync();
        } catch (LoginException | IllegalArgumentException ex) {
            ex.printStackTrace();
        }
        jda.addEventListener(new ReadyListener());
        jda.addEventListener(new MessageResponder());
        jda.addEventListener(new Music());
    }
}
