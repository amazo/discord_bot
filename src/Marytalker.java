import marytts.exceptions.SynthesisException;
import marytts.util.data.audio.MaryAudioUtils;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import javax.sound.sampled.AudioInputStream;
import java.io.IOException;

public class Marytalker {
    public  Marytalker(MessageReceivedEvent e) {
        VoiceChannel userChan = e.getGuild().getMember(e.getAuthor()).getVoiceState().getChannel();
        String message = e.getMessage().getContentRaw();
        if (userChan != null) {
            if (message.contains(" ")) {
                String[] messageSplit = message.split("\\s+", 2);
                if (messageSplit.length > 1) {
                    String outputFileName = "target/output.wav";
                    AudioInputStream audio = null;
                    try {
                        audio = Constants.mary.generateAudio(messageSplit[1]);
                    } catch (SynthesisException ex) {
                        System.err.println("Synthesis failed: " + e.getMessage());
                        System.exit(1);
                    }

                    // Write to output (saved in target/output.wav as indicated by outputFileName).
                    double[] samples = MaryAudioUtils.getSamplesAsDoubleArray(audio);
                    try {
                        MaryAudioUtils.writeWavFile(samples, outputFileName, audio.getFormat());
                        System.out.println("Output written to " + outputFileName);
                    } catch (IOException ex) {
                        System.err.println("Could not write to file: " + outputFileName + "\n" + e.getMessage());
                        System.exit(1);
                    }
                }
            }
        }
    }
}