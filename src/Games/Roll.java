package Games;

import java.util.Random;

public class Roll {
    public static String Throw(String message){//message format should follow .roll 3 d20, 4 d6
        //[# of rolls] d[number of faces]
        Random randomGenerator = new Random();
        String[] die;
        String[] dicerolls;
        String results;
        String msg;
        results="";
        int dicetype;
        int rolls;
        msg = message;
        die = msg.split(", ");
        if(die.length>10){
            return "you can only roll up to 10 dice.";
        }
        for(int i=0;i<die.length;i++){
            System.out.println(die[i]);
            if(!die[i].matches("^\\d{1,2}\\sd\\d{1,4}")){
                return "Incorrect format";
            }else{
                dicerolls=die[i].split("\\sd");//puts number of rolls in [0] and number of sides in [1]
                rolls=Integer.parseInt(dicerolls[0]);
                dicetype=Integer.parseInt(dicerolls[1]);
                if(rolls>20){
                    return "Too many rolls, limit is 20";
                }
                if(dicetype>100){
                    return "Dice type is too large, limit is D100";
                }
                results = results.concat("D"+dicetype+": ");//puts dice type before rolls
                for(int j=0;j<rolls;j++){//gets rolls for that amount of sides
                    results = results.concat((randomGenerator.nextInt(dicetype) +1)+" ");
                }
                results = results.concat("\n");
            }
        }
        return results;
    }
}