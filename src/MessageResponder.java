import Games.Gamble;
import Games.Roll;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class MessageResponder extends ListenerAdapter {
    @SuppressWarnings("unused")
    public void onMessageReceived (MessageReceivedEvent e) {
        if (!e.getMessage().getContentRaw().startsWith(Constants.prefix)) return;

        if (e.getChannelType()== ChannelType.PRIVATE){
            System.out.println((String.format("[DM] %s#%s: %s", e.getAuthor().getName(),e.getAuthor().getDiscriminator(),e.getMessage().getContentRaw())));
        } else {
            System.out.println(String.format("[%s][%s] %s#%s: %s",e.getGuild().getName(),e.getChannel().getName(),e.getAuthor().getName(), e.getAuthor().getDiscriminator(),e.getMessage().getContentRaw()));
        }

        String command = e.getMessage().getContentStripped().replace(Constants.prefix,"").split(" ")[0];
        String[] args = e.getMessage().getContentRaw().replace(Constants.prefix,"").replace(command,"").split(" ");

        if(command.equalsIgnoreCase("hello")){
            e.getChannel().sendMessage("Hello," + e.getAuthor().getName() + "!").queue();
        }else if (command.equalsIgnoreCase("gamble")){
            e.getChannel().sendMessage(Gamble.pull()).queue();
        }else if (command.equalsIgnoreCase("roll")){
            String s = (e.getMessage().getContentRaw().toLowerCase().replace(".roll ".toLowerCase(), ""));
            e.getChannel().sendMessage(Roll.Throw(s)).queue();
        }else if (command.equalsIgnoreCase("play")){
        }
    }
}
