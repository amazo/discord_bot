package Games;

import java.util.Random;

public class Gamble{
    public static String pull() {
        String[] roll1;
        String[] roll2;
        String[] roll3;

        roll1 = new String[]{":100:", ":fire:", ":french_bread:", ":slot_machine:",
                  ":eggplant:", ":sushi:",};
        roll2 = new String[]{":french_bread:",  ":sushi:", ":slot_machine:", ":100:", ":fire:", ":eggplant:",
                };
        roll3 = new String[]{":fire:", ":eggplant:", ":sushi:", ":slot_machine:",
                 ":french_bread:", ":100:", };
        Random randomGenerator = new Random();
        String row1;
        String row2;
        String row3;
        String result = ":slot_machine: SLOTS :slot_machine:\n";
        String winner = "";
        row1 = "";
        row2 = "";
        row3 = "";
        int po1 = randomGenerator.nextInt(10);
        int po2 = randomGenerator.nextInt(10);
        int po3 = randomGenerator.nextInt(10);
        row2 = roll1[po1] + roll2[po2] + roll3[po3];
        if (roll1[po1].contentEquals(roll2[po2]) || roll2[po2].contentEquals(roll3[po3])) {
            if (roll1[po1].contentEquals(roll2[po2]) && roll2[po2].contentEquals(roll3[po3])) {
                winner = "\n*LOUD NOISES*\nYou win maybe? IDK!";
            } else if ((roll1[po1].equals(":slot_machine:") && roll2[po2].equals(":slot_machine:")) || (roll2[po2].equals(":slot_machine:") && roll3[po3].equals(":slot_machine:"))) {
                winner = "\n*SLOT MACHINE IN SLOT MACHINE 10\\10*";
            } else if ((roll1[po1].equals(":eggplant:") && roll2[po2].equals(":eggplant:")) || (roll2[po2].equals(":eggplant:") && roll3[po3].equals(":eggplant:"))) {
                winner = "\n*Jura eggplant is the winner*\n";
            } else if ((roll1[po1].equals(":sushi:") && roll2[po2].equals(":sushi:")) || (roll2[po2].equals(":sushi:") && roll3[po3].equals(":sushi:"))) {
                winner = "\n*NANI!?*\nomae wa moui shindeiru'";
            } else if ((roll1[po1].equals(":fire:") && roll2[po2].equals(":fire:")) || (roll2[po2].equals(":fire:") && roll3[po3].equals(":fire:"))) {
                winner = "\n*'YOU'RE ON FIRE'*\n";
            } else {
                winner = "\n*The machine throws out the MONIES*";
            }
        } else {
            winner = "\n*Various slot machine noises*\nYou lost I think.";
        }
        if (po1 == 0 || po2 == 0 || po3 == 0) {//
            if (po1 == 0) {
                po1 = 10;
            }
            if (po2 == 0) {
                po2 = 10;
            }
            if (po3 == 0) {
                po3 = 10;
            }
        }
        row1 = roll1[po1 - 1] + roll2[po2 - 1] + roll3[po3 - 1];
        if (po1 >= 9 || po2 >= 9 || po3 >= 9) {
            if (po1 >= 9) {
                po1 = -1;
            }
            if (po2 >= 9) {
                po2 = -1;
            }
            if (po3 >= 9) {
                po3 = -1;
            }
        }
        row3 = roll1[po1 + 1] + roll2[po2 + 1] + roll3[po3 + 1];
        result = result + row1 + "\n" + row2 + "\n" + row3 + winner;
        return result;
    }

}
